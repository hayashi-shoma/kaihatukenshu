<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content='width=device-width, initial-scale=1'>
  <title>条件分け</title>
</head>
<body>
  <?php

  // idを設定
  $id = "hogehoge";
  // パスワードを設定
  $password = "12345";

  if($_POST['id'] == $id AND $_POST['pass'] == $password){
    /* ログイン成功の処理を書く */
    echo "ログイン成功";
  } else {
    /* ログイン失敗の処理を書く */
    echo "ログインできません";
  }
  ?>
</body>
</html>

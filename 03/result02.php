<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content='width=device-width, initial-scale=1'>
  <title>条件分け</title>
</head>
<body>
  <?php
  // ゴールド会員
  $gold_pw = "12345";
  $gold_id = "hogehogegold";

  // ノーマル会員
  $reg_pw = "abcde";
  $reg_id = "hogehogeregular";

  // 分岐の処理を書く
  if( $_POST['pass'] == $gold_pw AND $_POST['id'] ==  $gold_id){
    echo "ゴールド会員ログイン成功";
  }elseif( $_POST['pass'] == $reg_pw AND $_POST['id'] == $reg_id ){
    echo "ノーマル会員ログイン成功";
  }else{
    echo "ログインに失敗しました";
  }

  ?>
</body>
</html>

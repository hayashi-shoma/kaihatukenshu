<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content='width=device-width, initial-scale=1'>
  <title>指定行数でテーブルを書いてみよう</title>
</head>
<body>
    <form method='get' action='loop03.php'>
        <input type="number" name="namae01">行×</input>
        <input type="number" name="namae02">列</input>
        <br>
        <input class="bottom" type="submit"></input>
        <input class="bottom"type="reset"></input>
    </form>
    <table border="1">
        <?php
            for ($i=1; $i <= $_GET['namae01']; $i++) {
                echo "<tr>";
                for ($j=1; $j <= $_GET['namae02']; $j++) {
                    echo "<td>".$i."-".$j."</td>";
                }
                echo "</tr>";
            }
        ?>

    </table>
</body>
</html>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content='width=device-width, initial-scale=1'>
  <title>配列の基本</title>
</head>
<body>
    <table border="1">
        <?php
            $team01 = array("りんご","みかん","ブドウ","グレープフルーツ","イチゴ");
            $team02 = array("きゅうり","白菜","キャベツ","にんじん","玉ねぎ");
            $team03 = array("牛肉","豚肉","鶏肉","鹿肉","鴨肉");
            $team04 = array("鯛","鮪","鯖","鯵","鰆");
            $team05 = array("水","お茶","コーヒー","ジュース","牛乳");

            $total = array($team01, $team02, $team03, $team04, $team05);

                foreach ($total as $i) {
                    echo "<tr>";
                    foreach ($i as $total02) {
                        echo "<td>".$total02."</td>";
                    }
                    echo "</tr>";
                }

        ?>
    </table>


</body>
</html>

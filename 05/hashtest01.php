<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content='width=device-width, initial-scale=1'>
  <title>配列の基本</title>
</head>
<body>
    <pre>
        <?php
            $iro = array(
                'TV' => '笑ってコラえて！',
                '漫画' => 'ジャンプ',
                'アニメ' => '鬼滅',
                'ゲーム' => '原神'
                );

            echo $iro['TV'];
            echo $iro['漫画'];
            echo $iro['アニメ'];
            echo $iro['ゲーム'];
            $iro['漫画'] = 'サンデー';
            var_dump($iro);
        ?>
    </pre>
</body>
</html>

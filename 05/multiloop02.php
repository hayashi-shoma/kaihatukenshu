<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content='width=device-width, initial-scale=1'>
  <title>配列の基本</title>
</head>
<body>
    <table border="1">
        <?php
            $players = array(
                array(
                    "id" => "3",
                    "name" => "梶谷隆幸",
                    "position" => "外野手",
                    "from" => "島根",
                    "year" => "2007",
                ),
                array(
                    "id" => "44",
                    "name" => "佐野恵太",
                    "position" => "外野手",
                    "from" => "岡山",
                    "year" => "2017",
                ),
                array(
                    "id" => "15",
                    "name" => "井納翔一",
                    "position" => "投手",
                    "from" => "東京",
                    "year" => "2013",
                )
            );
        ?>
            <tr>
                <th>背番号</th>
                <th>名前</th>
                <th>ポジション</th>
                <th>出身地</th>
                <th>入団年</th>
            </tr>
        <?php
            foreach($players as $each){
                echo "<tr>";
                echo "<td>" . $each['id'] ."</td>"
                     ."<td>". $each['name'] . "</td>"
                     ."<td>". $each['position'] ."</td>"
                     ."<td>". $each['from'] ."</td>"
                     ."<td>". $each['year'] . "</td>";
                echo "</tr>";
            }
        ?>





    </table>


</body>
</html>

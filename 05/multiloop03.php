<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content='width=device-width, initial-scale=1'>
  <title>配列の基本</title>
</head>
<body>
    <table border="1" style="border-collapse:collapse;">
        <?php
            $Kpop = array(
                array(
                    "debut" => "2015",
                    "name" => "iKon",
                    "song" => "LOVE SCENARIO",
                    "number" => "7",
                ),
                array(
                    "debut" => "2006",
                    "name" => "BigBang",
                    "song" => "BANG BANG BANG",
                    "number" => "5",
                ),
                array(
                    "debut" => "2014",
                    "name" => "winner",
                    "song" => "REALLY REALLY",
                    "number" => "5",
                )
            );
         ?>

            <tr>
                <th>デビュー日</th>
                <th>グループ名</th>
                <th>代表曲</th>
                <th>メンバー数</th>
            </tr>

            <?php
            foreach($Kpop as $each){
                echo "<tr>";
                echo "<td>" . $each['debut'] ."</td>"
                     ."<td>". $each['name'] . "</td>"
                     ."<td>". $each['song'] ."</td>"
                     ."<td>". $each['number'] ."</td>";
                echo "</tr>";
                }




            ?>
    </table>


</body>
</html>

<!DOCTYPE html>
<html>
 <head>
  <meta charset="utf-8">
  <meta name="viewport" content='width=device-width, initial-scale=1'>
  <title>第二回課題、フォーム部品練習</title>
 </head>
 <body>
    <?php
    echo "会員登録ありがとうございます!!"
    ?><br>

    <?php
    echo "以下の内容で登録受付ました"
     ?><br>

     <table border="1" style="border-collapse:collapse;">
         <tr>
             <td>
                <?php echo "氏名： ". $_POST['namae']?>
            </td>
        </tr>

        <tr>
            <td>
                <?php echo "フリガナ： ". $_POST['hurigana']?>
            </td>
        </tr>

        <tr>
            <td>
                <?php echo "住所： ". $_POST['home']?>
            </td>
        </tr>

        <tr>
            <td>
                <?php echo "電話番号： ". $_POST['phone']?>
            </td>
        </tr>

        <tr>
            <td>
                <?php echo "メールアドレス： ". $_POST['mail1']?>
                <?php echo "@". $_POST['mail2']?>
            </td>
        </tr>
    </table>

 </body>
</html>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content='width=device-width, initial-scale=1'>
  <title>消費税計算のページを作ってみよう</title>
</head>

<body>


  <form method='post' action='tax.php'>
  <h1>消費税計算ページを作ってみよう</h1>
  <table border="1" style="border-collapse:collapse;">
    <tr>
      <th>商品名</th>
      <th>税抜価格（単位：円）</th>
      <th>個数</th>
      <th>税率</th>
    </tr>
    <tr>
      <td>
        <input type="text" name="ringo">
      </td>
      <td>
        <input type="text" name="kakaku1">
      </td>
      <td>
        <input type="text" name="kosu1">個
      </td>
      <td>
        <input type="radio" name="zeiritu1" value="0.08%">8%
        <input type="radio" name="zeiritu1" value="0.1%">10%
      </td>
    </tr>
    <tr>
      <td>
        <input type="text" name="hot">
      </td>
      <td>
        <input type="text" name="kakaku2">
      </td>
      <td>
        <input type="text" name="kosu2">個
      </td>
      <td>
        <input type="radio" name="zeiritu2" value="0.08%">8%
        <input type="radio" name="zeiritu2" value="0.1%">10%
      </td>
    </tr>
    <tr>
      <td>
        <input type="text" name="teba">
      </td>
      <td>
        <input type="text" name="kakaku3">
      </td>
      <td>
        <input type="text" name="kosu3">個
      </td>
      <td>
        <input type="radio" name="zeiritu3" value="0.08%">8%
        <input type="radio" name="zeiritu3" value="0.1%">10%
      </td>
    </tr>
    <tr>
      <td>
        <input type="text" name="kawa">
      </td>
      <td>
        <input type="text" name="kakaku4">
      </td>
      <td>
        <input type="text" name="kosu4">個
      </td>
      <td>
        <input type="radio" name="zeiritu4" value="0.08%">8%
        <input type="radio" name="zeiritu4" value="0.1%">10%
      </td>
    </tr>
    <tr>
      <td>
        <input type="text" name="takeno">
      </td>
      <td>
        <input type="text" name="kakaku5">
      </td>
      <td>
        <input type="text" name="kosu5">個
      </td>
      <td>
        <input type="radio" name="zeiritu5" value="0.08%">8%
        <input type="radio" name="zeiritu5" value="0.1%">10%
      </td>
    </tr>
  </table>
  <input type="submit" value="送信">
  <input type="submit" value="キャンセル">
  <br>

 
</form>
<br>
<!--一行目の小計計算式-->
<?php
  $total1 = $_POST['kakaku1'];

  $total1 = $total1 * (1+$_POST['zeiritu1']);
  $total1 = $total1 * $_POST['kosu1'];
?>
<!--二行目の小計計算式-->
<?php
$total2 = $_POST['kakaku2'];
$total2 = $total2 * (1+$_POST['zeiritu2']);
$total2 = $total2 * $_POST['kosu2'];
?>
<!--三行目の小計計算式-->
<?php
$total3 = $_POST['kakaku3'];
$total3 = $total3 * (1+$_POST['zeiritu3']);
$total3 = $total3 * $_POST['kosu3'];
?>
<!--四行目の小計計算式-->
<?php
$total4 = $_POST['kakaku4'];
$total4 = $total4 * (1+$_POST['zeiritu4']);
$total4 = $total4 * $_POST['kosu4'];
?>
<!--五行目の小計計算式-->
<?php
$total5 = $_POST['kakaku5'];
$total5 = $total5 * (1+$_POST['zeiritu5']);
$total5 = $total5 * $_POST['kosu5'];
?>
<!--合計の計算式-->
<?php
$totalprice = $total1 + $total2 + $total3 + $total4 + $total5
?>

  <table border="1" style="border-collapse:collapse;">
    <tr>
      <th>商品名</th>
      <th>税抜価格（単位：円）</th>
      <th>個数</th>
      <th>税率</th>
      <th>小計（単位：円）</th>
    </tr>
<!--一行目の入力フォーム-->
    <tr>
      <td><?php echo $_POST["ringo"];?></td>
      <td><?php echo $_POST["kakaku1"];?></td>
      <td><?php echo $_POST["kosu1"];?></td>
      <td><?php echo $_POST["zeiritu1"]*100;?>%</td>
      <td><?php echo round ($total1);?></td>
    </tr>

<!--二行目の入力フォーム-->
    <tr>
      <td><?php echo $_POST["hot"];?></td>
      <td><?php echo $_POST["kakaku2"];?></td>
      <td><?php echo $_POST["kosu2"];?></td>
      <td><?php echo $_POST["zeiritu2"]*100;?>%</td>
      <td><?php echo round ($total2);?></td>
    </tr>

<!--三行目の入力フォーム-->
    <tr>
      <td><?php echo $_POST["teba"];?></td>
      <td><?php echo $_POST["kakaku3"];?></td>
      <td><?php echo $_POST["kosu3"];?></td>
      <td><?php echo $_POST["zeiritu3"]*100;?>%</td>
      <td><?php echo round ($total3);?></td>
    </tr>

<!--四行目の入力フォーム-->
    <tr>
      <td><?php echo $_POST["kawa"];?></td>
      <td><?php echo $_POST["kakaku4"];?></td>
      <td><?php echo $_POST["kosu4"];?></td>
      <td><?php echo $_POST["zeiritu4"]*100;?>%</td>
      <td><?php echo round ($total4);?></td>
    </tr>

<!--五行目の入力フォーム-->
    <tr>
      <td><?php echo $_POST["takeno"];?></td>
      <td><?php echo $_POST["kakaku5"];?></td>
      <td><?php echo $_POST["kosu5"];?></td>
      <td><?php echo $_POST["zeiritu5"]*100;?>%</td>
      <td><?php echo round ($total5);?></td>
    </tr>

<!--合計入力フォーム-->


    <td align="" colspan="4"><strong>合計</strong></td>
    <td><?php echo $totalprice ?> 円 </td>
  </table>
</body>
</html>

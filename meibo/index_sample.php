<!DOCTYPE html>
<?php
$DB_DSN = "mysql:host=localhost; dbname=shayashi; charset=utf8";
 $DB_USER = "webaccess";
 $DB_PW = "toMeu4rH";
 $pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);


 $query_str = "SELECT * FROM test_table";   // 実行するSQL文を作成して変数に保持

 echo $query_str;                                                           // 実行するSQL文を画面に表示するだけ（デバッグプリント
 $sql = $pdo->prepare($query_str);                              // PDOオブジェクトにSQLを渡す
 $sql->execute();                                                            // SQLを実行する
 $result = $sql->fetchAll();
 ?>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content='width=device-width, initial-scale=1'>
  <title>居酒屋メニュー</title>
</head>
<body>
    <pre>
      <?php
      var_dump($result);
       ?>
    </pre>

    <?php
    foreach($result as $each){
      // var_dump($each);
      echo $each['DishName'] . " : " . $each['price'] . " 円";
      echo "<hr/>";
    }
    ?>

</body>
</html>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content='width=device-width, initial-scale=1'>
  <title>配列の基本</title>
</head>
<body>
    <pre>

        <?php

        $irooni = array("赤", "青", "緑", "黄", "紫", "白");

            echo $irooni[3];

            echo $irooni[0];

            $irooni[0] = "red";

            $irooni[6] = "yellow";

            $irooni[7] = "black";

            var_dump($irooni);

            for ($i=0; $i < count($irooni); $i++) {
                echo $irooni[$i] . "</br>";
            }


        ?>
    </pre>
</body>
</html>

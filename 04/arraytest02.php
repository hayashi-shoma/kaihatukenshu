<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content='width=device-width, initial-scale=1'>
  <title>foreachの利用</title>
</head>
<body>
    <?php
        $iro = array("赤", "青", "緑", "黄", "紫", "白");
        var_dump($iro);

        echo "<hr>";

        foreach($iro as $irooni){
            echo $irooni . "<br/>";
        }

        echo "<hr>";

        foreach($iro as $key => $value){
            echo $key . "番目の要素は" . $value . "です。<br/>";
        }

        $search = "kuro";

        if(in_array($search,$iro)){
            echo $search . "がiroの要素の値に存在しています";
        }else{
            echo $search . "がiroの要素の値に存在しません";
           }
    ?>
</body>
</html>
